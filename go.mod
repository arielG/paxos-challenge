module gitlab.com/arielG/paxos-go-challenge

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.7.3
)
