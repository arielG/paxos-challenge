package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/arielG/paxos-go-challenge/account"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var httpAdrr = flag.String("http", ":8080", "http listen address")
	logger := getLogger()

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	flag.Parse()

	ctx := context.Background()

	userService := getAccountService(logger)

	errors := getErrors()

	endpoints := account.MakeEndpoints(userService)

	errors = listenForErrors(httpAdrr, ctx, endpoints, errors)

	level.Error(logger).Log("exit", <-errors)
}

func listenForErrors(httpAdrr *string, ctx context.Context, endpoints account.Endpoints, errors chan error) chan error {
	go func() {
		fmt.Println("listening on port", *httpAdrr)
		handler := account.NewHttpServer(ctx, endpoints)
		errors <- http.ListenAndServe(*httpAdrr, handler)
	}()
	return errors
}

func getErrors() chan error {
	errors := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errors <- fmt.Errorf("%s", <-c)
	}()
	return errors
}

func getAccountService(logger log.Logger) account.UserService {
	var userService account.UserService
	{
		userRepository := account.NewUserRepository(logger)
		userService = account.NewUserService(userRepository, logger)
	}
	return userService
}

func getLogger() log.Logger {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "account",
			"time:", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller)
	}
	return logger
}
