package account

import (
	"context"
	"errors"
	"github.com/go-kit/kit/log"
	"github.com/gofrs/uuid"
)

var userDB = []User{
	{
		ID:      "1",
		Email:   "ariel.ep91@gmail.com",
		Balance: 0,
	},
}

var transactionDB = make([]Transaction, 0)

type accountRepository struct {
	logger log.Logger
}

func NewUserRepository(logger log.Logger) UserRepository {
	return &accountRepository{
		logger: logger,
	}
}

func (ur accountRepository) GetUser(ctx context.Context) (User, error) {
	logger := log.With(ur.logger, "method", "GetUser")
	_ = logger.Log("")
	userFound := userDB[0]
	_ = logger.Log("user found", userFound)
	return userFound, nil
}

func (ur accountRepository) CreateTransaction(ctx context.Context, transaction Transaction) (Transaction, error) {
	logger := log.With(ur.logger, "method", "CreateTransaction")
	uid, _ := uuid.NewV4()
	transaction.ID = uid.String()
	transactionDB = append(transactionDB, transaction)
	_ = logger.Log("transaction created successfully")
	return transaction, nil
}

func (ur accountRepository) GetTransactionHistory(ctx context.Context) ([]Transaction, error) {
	logger := log.With(ur.logger, "method", "GetTransactionHistory")
	_ = logger.Log("transaction history retrieved successfully")
	return transactionDB, nil
}

func (ur accountRepository) GetTransactionById(ctx context.Context, id string) (Transaction, error) {
	logger := log.With(ur.logger, "method", "GetTransactionById")
	transaction := filterTransaction(id)
	if transaction.ID == "" {
		_ = logger.Log("transaction not found", id)
		return Transaction{}, errors.New("transaction not found")
	} else {
		_ = logger.Log("transaction found", transaction)
		return transaction, nil
	}
}

func filterTransaction(id string) Transaction {
	for _, t := range transactionDB {
		if t.ID == id {
			return t
		}
	}
	return Transaction{}
}
