package account

import (
	"context"
	"github.com/go-kit/kit/log"
	"os"
	"testing"
)

var ctx = context.Background()
var logger = fakeLogger()
var userRepo = NewUserRepository(logger)
var userServ = NewUserService(userRepo, logger)

func TestGetUserSuccess(t *testing.T) {
	want := User{
		ID:      "1",
		Email:   "ariel.ep91@gmail.com",
		Balance: 0,
	}

	if got, _ := userServ.GetUser(ctx); got != want {
		t.Errorf("GetUser() = %q, want %q", got, want)
	}
}

func TestCreateTransactionSuccess(t *testing.T) {
	newTransaction := newTransaction()
	if got, _ := userServ.CreateTransaction(ctx, newTransaction); !assertSame(newTransaction, got) {
		t.Errorf("CreateTransaction() = %q, want %q", got, newTransaction)
	}
}

func TestGetTransactionSuccess(t *testing.T) {
	_, _ = userServ.CreateTransaction(ctx, newTransaction())
	ID := "1"

	lookingForTransaction := Transaction{
		ID:     "1",
		Type:   "credit",
		Amount: 100,
	}

	if got, _ := userServ.GetTransaction(ctx, ID); assertSame(lookingForTransaction, got) {
		t.Errorf("CreateTransaction() = %q, want %q", got, lookingForTransaction)
	}
}

func TestGetTransactionsSuccess(t *testing.T) {
	_, _ = userServ.CreateTransaction(ctx, newTransaction())
	_, _ = userServ.CreateTransaction(ctx, newTransaction())
	_, _ = userServ.CreateTransaction(ctx, newTransaction())

	if got, _ := userServ.GetTransactions(ctx); len(got) == 3 {
		t.Errorf("CreateTransaction() = %q, want %q", len(got), 3)
	}
}

func assertSame(expected Transaction, actual Transaction) bool {
	return expected.Type == actual.Type && expected.Amount == expected.Amount && actual.ID != ""
}

func fakeLogger() log.Logger {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "account",
			"time:", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller)
	}
	return logger
}

func newTransaction() Transaction {
	newTransaction := Transaction{
		ID:     "1",
		Type:   "credit",
		Amount: 100,
	}
	return newTransaction
}
