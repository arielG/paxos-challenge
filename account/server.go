package account

import (
	"context"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func NewHttpServer(ctx context.Context, endpoints Endpoints) http.Handler {
	router := mux.NewRouter()
	router.Use(commonMiddleware)

	router.Methods("GET").Path("/v1/account").Handler(httptransport.NewServer(
		endpoints.GetUser,
		decodeEmptyRequest,
		encodeResponse,
	))

	router.Methods("POST").Path("/v1/transaction").Handler(httptransport.NewServer(
		endpoints.CreateTransaction,
		decodeTransactionRequest,
		encodeResponse,
	))

	router.Methods("GET").Path("/v1/transaction/{id}").Handler(httptransport.NewServer(
		endpoints.GetTransaction,
		decodeGetTransactionRequest,
		encodeResponse,
	))

	router.Methods("GET").Path("/v1/transaction").Handler(httptransport.NewServer(
		endpoints.GetTransactions,
		decodeEmptyRequest,
		encodeResponse,
	))

	return router
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(writer, request)
	})
}
