package account

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

type (
	GetTransactionRequest struct {
		ID string `json:"id"`
	}

	CreateTransactionRequest struct {
		Type   Type  `json:"type"`
		Amount int64 `json:"amount"`
	}

	CreateTransactionResponse struct {
		ID     string `json:"id"`
		Type   Type   `json:"type"`
		Amount int64  `json:"amount"`
	}

	GetUserResponse struct {
		ID      string `json:"id"`
		Email   string `json:"email"`
		Balance int64  `json:"balance"`
	}

	GetTransactionResponse struct {
		ID     string `json:"id"`
		Type   Type   `json:"type"`
		Amount int64  `json:"amount"`
	}

	GetTransactionsResponse struct {
		Transactions []Transaction `json:"transactions"`
	}

	ErrorResponse struct {
		Error string `json:"error"`
	}

	EmptyRequest struct {
	}
)

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func decodeGetTransactionRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	var req GetTransactionRequest
	vars := mux.Vars(request)

	req = GetTransactionRequest{
		ID: vars["id"],
	}

	return req, nil
}

func decodeTransactionRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	var req CreateTransactionRequest
	err := json.NewDecoder(request.Body).Decode(&req)

	if err != nil {
		return nil, err
	}

	return req, nil
}

func decodeEmptyRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	return EmptyRequest{}, nil
}
