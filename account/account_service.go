package account

import "context"

type UserService interface {
	GetUser(ctx context.Context) (User, error)
	CreateTransaction(ctx context.Context, transaction Transaction) (Transaction, error)
	GetTransaction(ctx context.Context, id string) (Transaction, error)
	GetTransactions(ctx context.Context) ([]Transaction, error)
}
