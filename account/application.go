package account

import (
	"context"
	"errors"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

type userService struct {
	repository UserRepository
	logger     log.Logger
}

func NewUserService(userRepo UserRepository, logger log.Logger) UserService {
	return &userService{
		repository: userRepo,
		logger:     logger,
	}
}

// User service logic
func (us userService) GetUser(ctx context.Context) (User, error) {
	logger := log.With(us.logger, "method", "GetUser")
	user, err := us.repository.GetUser(ctx)
	if err != nil {
		_ = level.Error(logger).Log("err", err)
		return User{}, err
	}
	_ = logger.Log("user can be retrieved")
	return user, nil
}

// Transaction Logic
func (us userService) CreateTransaction(ctx context.Context, transaction Transaction) (Transaction, error) {
	logger := log.With(us.logger, "method", "CreateTransaction")
	err := us.updateBalance(ctx, transaction)
	if err != nil {
		return Transaction{}, err
	}
	newTransaction, err := us.repository.CreateTransaction(ctx, transaction)
	if err != nil {
		_ = level.Error(logger).Log("err", err)
		return Transaction{}, err
	}
	_ = logger.Log("transaction can be created", newTransaction)
	return newTransaction, nil

}

func (us userService) GetTransaction(ctx context.Context, id string) (Transaction, error) {
	logger := log.With(us.logger, "method", "GetTransaction")
	transaction, err := us.repository.GetTransactionById(ctx, id)
	if err != nil {
		_ = level.Error(logger).Log("err", err)
		return Transaction{}, err
	}
	_ = logger.Log("transaction can be retrieved", id)
	return transaction, nil

}

func (us userService) GetTransactions(ctx context.Context) ([]Transaction, error) {
	logger := log.With(us.logger, "method", "GetTransactions")
	transactions, err := us.repository.GetTransactionHistory(ctx)
	if err != nil {
		_ = level.Error(logger).Log("err", err)
		return []Transaction{}, err
	}
	_ = logger.Log("transaction can be retrieved", transactions)
	return transactions, nil
}

func (us userService) updateBalance(ctx context.Context, transaction Transaction) error {
	user, err := us.GetUser(ctx)
	if err == nil {
		switch transaction.Type {
		case Credit:
			userDB[0].Balance = user.Balance + transaction.Amount
		case Debit:
			newBalance := user.Balance - transaction.Amount
			if newBalance < 0 {
				return errors.New("negative balance")
			} else {
				userDB[0].Balance = user.Balance - transaction.Amount
			}

		default:
			return errors.New("transactions unregistered")
		}
	}
	return nil
}
