package account

import "context"

type User struct {
	ID      string `json:"id,omitempty"`
	Email   string `json:"email"`
	Balance int64  `json:"balance"`
}

type Transaction struct {
	ID     string `json:"id,omitempty"`
	Type   Type   `json:"type"`
	Amount int64  `json:"amount"`
}

type Type string

const (
	Credit Type = "credit"
	Debit  Type = "debit"
)

type UserRepository interface {
	GetUser(ctx context.Context) (User, error)
	CreateTransaction(ctx context.Context, transaction Transaction) (Transaction, error)
	GetTransactionHistory(ctx context.Context) ([]Transaction, error)
	GetTransactionById(ctx context.Context, id string) (Transaction, error)
}
