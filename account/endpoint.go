package account

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	GetUser           endpoint.Endpoint
	CreateTransaction endpoint.Endpoint
	GetTransaction    endpoint.Endpoint
	GetTransactions   endpoint.Endpoint
}

func MakeEndpoints(userService UserService) Endpoints {
	return Endpoints{
		GetUser:           makeGetUserEndpoint(userService),
		CreateTransaction: makeCreateTransaction(userService),
		GetTransaction:    makeGetTransaction(userService),
		GetTransactions:   makeGetTransactions(userService),
	}
}

func makeGetUserEndpoint(userService UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		user, err := userService.GetUser(ctx)
		if err == nil {
			return GetUserResponse{
				ID:      user.ID,
				Email:   user.Email,
				Balance: user.Balance,
			}, err
		} else {
			return ErrorResponse{
				Error: "user not found",
			}, err
		}
	}
}

func makeCreateTransaction(userService UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(CreateTransactionRequest)
		transaction := Transaction{
			Type:   req.Type,
			Amount: req.Amount,
		}
		newTransaction, err := userService.CreateTransaction(ctx, transaction)
		if err == nil {
			return CreateTransactionResponse{
				ID:     newTransaction.ID,
				Type:   newTransaction.Type,
				Amount: newTransaction.Amount,
			}, err
		} else {
			return ErrorResponse{
				Error: "transaction could not be created",
			}, err
		}
	}
}

func makeGetTransaction(userService UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetTransactionRequest)
		newTransaction, err := userService.GetTransaction(ctx, req.ID)
		if err == nil {
			return GetTransactionResponse{
				ID:     newTransaction.ID,
				Type:   newTransaction.Type,
				Amount: newTransaction.Amount,
			}, err
		} else {
			return ErrorResponse{
				Error: "transaction could not be created",
			}, err
		}
	}
}

func makeGetTransactions(userService UserService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		transactionsHistory, err := userService.GetTransactions(ctx)
		if err == nil {
			return GetTransactionsResponse{
				Transactions: transactionsHistory,
			}, err
		} else {
			return ErrorResponse{
				Error: "transaction could not be created",
			}, err
		}
	}
}
